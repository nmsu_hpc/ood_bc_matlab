# Batch Connect - MATLAB

A Batch Connect app designed for OSC OnDemand that launches MATLAB within an
Owens batch job.

## Prerequisites

This Batch Connect app requires the following software be installed on the
**compute nodes** that the batch job is intended to run on (**NOT** the
OnDemand node):

- [MATLAB] R2016b+
- [Xfce Desktop] 4+

For VNC server support:

- [TurboVNC] 2.1+
- [websockify] 0.8.0+

For hardware rendering support:

- [X server]
- [VirtualGL] 2.3+

**Optional** software:

- [Lmod] 6.0.1+ or any other `module purge` and `module load <modules>` based
  CLI used to load appropriate environments within the batch job

[MATLAB]: https://www.mathworks.com/
[Xfce Desktop]: https://xfce.org/
[TurboVNC]: http://www.turbovnc.org/
[websockify]: https://github.com/novnc/websockify
[X server]: https://www.x.org/
[VirtualGL]: http://www.virtualgl.org/
[Lmod]: https://www.tacc.utexas.edu/research-development/tacc-projects/lmod

## Site Customizations

- `form.js`
  - Optional and can be removed.
- `form.yml.erb`
  - Values need to be updated to reflect your site.
  - The dynamic partition list may or maynot work for you.
- `partitions.json`
  - Partition values for dynamic partition list in form.yml.erb -> form 'custom_queue'.
- `modules.sh`
  - You'll need to adjust what modules are loaded.
- `sif.sh`
  - We install apptainer globally but if you need to load modules you can add that here.
- `manifest.yml`
  - links to documentation.
- `submit.yml.erb`
  - Adjujst parameters to match the scheduler/settings at your site.
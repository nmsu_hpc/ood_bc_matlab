#!/usr/bin/env bash

# Load Initial Modules
# shellcheck disable=SC2086
module load $OOD_MODULES

# Report Modules
echo ""
echo "The Follow Modules Are Loaded:"
module --terse list
echo ""
